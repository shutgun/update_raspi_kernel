#!/bin/bash -e

source /lib/gentoo/functions.sh

cd $(dirname $(realpath $0))

if [ -f update.cfg ]; then
	if egrep -q -v '(^\s*#|^\s*$|^(UPDATE_HOSTS|REBOOT_TIME|EXCLUDE_FILES)=[\(\)\"a-z.0-9\ \-]*$)' update.cfg; then
		eerror "Configfile contains unexpected content!"
	fi
else
	eerror "Configfile is missing!"
fi

source update.cfg

# Download latest firmware from master branch
mkdir firmware
cd firmware
einfo "Downloading firmware ..."
wget --quiet --show-progress --output-document=firmware.zip https://github.com/raspberrypi/firmware/archive/master.zip

# Extract firmware
ebegin "Extracting archive"
unzip -q firmware.zip
eend

# Check content
cd firmware-master
if [ ! -d boot ] || [ ! -d modules ]; then
	cd ../..
	rm -R firmware
	eerror "Archive messed up! Either kernel or modules missing."
fi
cd ..

EXCLUDE_COMMAND="--exclude *.txt "
for EXCLUDE_FILE in ${EXCLUDE_FILES[@]}; do
	EXCLUDE_COMMAND="${EXCLUDE_COMMAND} --exclude ${EXCLUDE_FILE} "
done

for UPDATE_HOST in ${UPDATE_HOSTS[@]}; do

	# Copy kernel and modules
	cd firmware-master
	einfo "Copying kernel to $UPDATE_HOST ..."
	rsync --rsh "ssh" --archive --info=progress2 --delete ${EXCLUDE_COMMAND} boot root@$UPDATE_HOST:/
	einfo "Copying modules to $UPDATE_HOST ..."
	rsync --rsh "ssh" --archive --info=progress2 --delete modules root@$UPDATE_HOST:/lib

	# Reboot
	ebegin "Rebooting $UPDATE_HOST"
	ssh root@$UPDATE_HOST 'reboot'
	sleep $REBOOT_TIME
	if ncat -z $UPDATE_HOST 22; then
		eend
	else
		cd ../..
	        rm -R firmware
		eend 1
	fi
	cd ..

	# Download kernel sources matsching version of running kernel, if we do not have them already
	VERSION=`ssh root@$UPDATE_HOST 'uname -r' | cut -d. -f1,2`
	if [ ! -f linux-$VERSION.zip ]; then
		ebegin "Downloading sources for $UPDATE_HOST ..."
		wget --quiet --show-progress --output-document=linux-$VERSION.zip https://github.com/raspberrypi/linux/archive/rpi-$VERSION.y.zip

		ebegin "Extracting archive"
		unzip -q linux-$VERSION.zip
		eend
	fi

	# Copy sources
	einfo "Copying sources to $UPDATE_HOST ..."
	rsync --rsh "ssh" --archive --info=progress2 --delete linux-rpi-$VERSION.y/ root@$UPDATE_HOST:/usr/src/linux
	ssh root@$UPDATE_HOST 'modprobe configs'
	ssh root@$UPDATE_HOST 'zcat /proc/config.gz > /usr/src/linux/.config'
	ssh root@$UPDATE_HOST 'modprobe -r configs'

done

# Remove extracted directories
ebegin "Cleaning"
cd ..
rm -R firmware
eend
