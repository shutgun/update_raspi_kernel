# update_raspi_kernel

I run a Raspberry Pi with Gentoo. The kernel binaries as well as the kernel sources in portage are either outdated or full-blown git repos. I wanted a light solution and created this bash script to download both from the official GitHub repository and copy it to the Raspberry Pi.

## Getting started

In my script I assume that you run a managing PC also with Gentoo that will control the download and the copy process. Clone this repository on that PC and assure it has bash, wget, rsync and unzip installed. On the Raspberry Pi ensure that ssh and rsync are installed. Next, assure that you have passwordless access to the root user of the Raspberry Pi. Copy the example config file to update.cfg and edit the config file to your needs. Multiple Raspberry Pis are supported.

## Usage

The bash script does not take parameters. Just execute it and watch it do the magic. After downloading and extracting the firmware it will sync the boot directory and the modules directory. Next it will reboot the Raspberry Pi. After discovering the newly booted kernel version it will download, extract and sync the correspondent sources. By using the module configs it will save the running kernel config so that Gentoo ebuild can check it, if necessary.
